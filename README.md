JavaScript EJS Editor
=====================

JavaScript EJS is the latest version of Easy JavaScript Simulations that creates HTML+JavaScript simulations.

Versions
========

The release version is the latest stable version and ready to use.

The beta version is the latest pre-release version with new features.

Installation
============

JavaScript EJS is compatible with Java 8 and later.

[**Recomended**] Using Java 8:
------------------------------
* Download and install JRE 8 from [here](https://www.java.com/download/).
* Download and unzip the EJS distribution file in any suitable directory of your computer.
* Double-click on EjsConsole.jar.
 
Using Java 11 and later (only beta version):
-------------------------------------------
* Download and install JDK from [here](https://www.oracle.com/java/technologies/javase-downloads.html).
* Download and zip JavaFX from [here](https://gluonhq.com/products/javafx/).
* Download and unzip the EJS distribution file in any suitable directory of your computer.
* Open a terminal in the directory of the EJS distribution and execute the following command:
    * (Mac or Linux) ``` java -cp 'path_to_javafx\javafx-sdk-11.0.2\lib\*:*' org.ejs.console.EjsConsole ```
    * (Windows) ``` java -cp 'path_to_javafx\javafx-sdk-11.0.2\lib\*;*' org.ejs.console.EjsConsole ```

As you can see, JavaScript EJS requires JavaFX (only integrated into Java 8). 

For more information about Java support, please visit [Oracle Java SE Support Roadmap](https://www.oracle.com/java/technologies/java-se-support-roadmap.html).